import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  countries: any;
  departments: any;
  empForm: FormGroup;

  constructor(private fb: FormBuilder, private service: EmpService, private router: Router) {
    this.empForm = this.fb.group({
      empName: ['', Validators.required],
      salary: ['', Validators.required],
      gender: ['', Validators.required],
      doj: ['', Validators.required],
      country: ['', Validators.required],
      emailId: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      department: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }

  registerSubmit() {
    if (this.empForm.valid) {
      const confirmPasswordControl = this.empForm.get('confirmPassword');

      if (confirmPasswordControl && confirmPasswordControl instanceof Object) {
        if (this.empForm.value.password === this.empForm.value.confirmPassword) {
          const formData = this.empForm.value;
          this.service.regsiterEmployee(formData).subscribe((data: any) => {
            console.log(data);
            // Navigate to login page only when registration is successful
            this.router.navigate(['login']);
          });
        } else {
          console.log('Password and Confirm Password must be the same.');
          confirmPasswordControl.setErrors({ mismatch: true });
        }
      }
    } else {
      console.log('Please fill in all the required fields.');
      this.empForm.markAllAsTouched();
    }
  }
} 