import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component  implements OnInit{

  person:any;

  constructor(){

    this.person = {
      id:101,
      name:"upendra",
      avg:22.1,

      address:{
        streetNo:56,
        city:'Hyderabad',
        state:"Telangana"
      },

      hobbies:['playing','running','swimming']
    }
  }

  ngOnInit() {
  }

  buttonSubmit(){
    alert("Button clicked");
    console.log(this.person);
  }

}
