import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  avg: number;

  address: any;
  hobbies: any;

  constructor(){
    //alert("constructor invoked...");

    this.id = 101;
    this.name = 'upendra';
    this.avg = 22.1;

    this.address = {
      streetNo: 102,
      city: 'Hyderabad',
      state:'Telangana'
    };

    this.hobbies = ['sleeping','Eating','Playing','Swimming'];
  }

  //when we are dealing with external apis then we go with it
  ngOnInit(){
   //alert("ngOnInit invoked..");
  }

}
