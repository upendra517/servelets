import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-showemployeebyid',
  templateUrl: './showemployeebyid.component.html',
  styleUrl: './showemployeebyid.component.css'
})
export class ShowemployeebyidComponent implements OnInit {

 
  empId: any;
  emp: any;


  constructor(private service:EmpService) {
    
  }

  ngOnInit() {
  }

  getEmpById(emp: any) {
        this.service.getEmployeeById(emp.empId).subscribe((data:any) => {
          this.emp = data;
        })
  }
  

}
