package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.Dbconnection;
import com.dto.Student;

public class StudentDao {

public Student stuLogin(String emailId, String password) {
		
		Connection con = Dbconnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student student = new Student();
				
				student.setStuId(rs.getInt(1));
				student.setStuName(rs.getString(2));
				student.setGender(rs.getString(3));
				student.setEmailId(rs.getString(4));
				student.setPassword(rs.getString(5));
				
				return student;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

}
